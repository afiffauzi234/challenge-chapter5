const express = require("express");
const router = express.Router();

router.use(express.static("public"));
router.use("/css", express.static(__dirname + "public/css"));
router.use("/js", express.static(__dirname + "public/js"));
router.use("/assets", express.static(__dirname + "public/assets"));

router.get("/", (req, res) => {
  res.render("index");
});

router.get("/game", (req, res) => {
  res.render("game");
});

router.get("/login", (req, res) => {
  res.render("login");
});

router.get("/signup", (req, res) => {
  res.render("signup");
});

module.exports = router;
