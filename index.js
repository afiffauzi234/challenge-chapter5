const express = require("express");
const app = express();
const port = 3000;
const router = require("./routers/index");
const UserManagement = require("./controllers/UserManagement/index");

app.set("view engine", "ejs");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(router);
app.use(UserManagement);

app.use("/", (req, res) => {
  res.status(404);
  res.json({
    status: "404",
    errors: "page not found",
  });
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
