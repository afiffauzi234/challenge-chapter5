const fs = require("fs");
const express = require("express");
const UserManagement = express.Router();
const database = require("../../database/user.json");

UserManagement.use(express.json());
UserManagement.use(express.urlencoded({ extended: false }));

UserManagement.post("/login", (req, res) => {
  let request = req.body;
  let userData = database;
  for (let i = 0; i < userData.length; i++) {
    const element = userData[i];
    if (request.username === element.username && request.password === element.password) {
      res.status(200);
      res.render("game");
      res.redirect("/game");
    } else {
      res.status(401);
      res.send("Invalid username or password!");
    }
  }
});

UserManagement.post("/signup", (req, res) => {
  let newData = {
    username: req.body.username,
    password: req.body.password,
  };
  let file = fs.readFileSync("../../database/user.json");
  let myObject = JSON.parse(file);
  myObject.push(newData);
  let data = JSON.stringify(myObject);
  fs.writeFileSync("../../database/user.json", data, (err) => {
    if (err) throw err;
    console.log(database);
  });
});

module.exports = UserManagement;
